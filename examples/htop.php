<?php
require __DIR__ . '/../vendor/autoload.php';

use Crunch\OneTimePassword as otp;

foreach (range(0, 9) as $count) {
    echo "$count  ";
    $code = otp\generateHMAC("12345678901234567890", $count, 6);

    echo "  $code\n";
}
