<?php
require __DIR__ . '/../vendor/autoload.php';

use Crunch\OneTimePassword as otp;
use Crunch\Base32 as base32;

$base = 'abcdefghijklmnop';


echo "You can compare this value with Google Authenticator. Register an account with the secret $base\n";
echo otp\generateTimebased(base32\decode('abcdefghijklmnop'), time(), 6) . "\n";

