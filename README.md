Crunch\OneTimeBundle
================

[![Latest Stable Version](https://poser.pugx.org/crunch/one-time-password/version.png)](https://packagist.org/packages/crunch/one-time-password)
[![Build Status](https://travis-ci.org/KingCrunch/OneTimePassword.png?branch=master)](https://travis-ci.org/KingCrunch/OneTimePassword)
[![Total Downloads](https://poser.pugx.org/crunch/one-time-password/d/total.png)](https://packagist.org/packages/crunch/one-time-password)

Generate one-time-passwords (aka OTP) both counter- and time-based. This library is
[RFC4226](http://tools.ietf.org/html/rfc4226) and [RFC6238](http://tools.ietf.org/html/rfc6238)
compliant. This includes, that you can use it with Google Authenticator.

It is currently under development, but works quite fine. The most important thing to get done is
stabilization.

Installation
============
Add `"crunch/one-time-password": "1.0.*@dev"` to your `composer.json`.

Quickstart
==========
Google Authenticator example. The shared token is `abcdefghijklmnop`.

    require __DIR__ . '/../vendor/autoload.php';

    use Crunch\OneTimePassword as otp;
    use Crunch\Base32 as base32;

    $secret = 'abcdefghijklmnop';

    $code = otp\generateTimebased(base32\decode($secret), time(), 6);

Requirements
============
* PHP => 5.3

Contributors
============
See CONTRIBUTING.md for details on how to contribute.

* Sebastian "KingCrunch" Krebs <krebs.seb@gmail.com> -- http://www.kingcrunch.de/ (german)

License
=======
This library is licensed under the MIT License. See the LICENSE file for details.

References
==========
* RFC4226 http://tools.ietf.org/html/rfc4226
* RFC6238 http://tools.ietf.org/html/rfc6238
