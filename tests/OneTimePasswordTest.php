<?php
namespace Crunch\OneTimePassword;

class OneTimePasswordsTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @dataProvider getHTOPProvider
     * @param $secret
     * @param $movingFactor
     * @param $digits
     * @param $expectedCode
     */
    public function testHmacOneTimePasswords ($secret, $movingFactor, $digits, $expectedCode)
    {
        $code = generateHMAC($secret, $movingFactor, $digits);
        $this->assertEquals($expectedCode, $code);
    }

    /**
     * @dataProvider getTOTPProvider
     * @param $secret
     * @param $timestamp
     * @param $digits
     * @param $expectedCode
     * @param $crypt
     */
    public function testTimebasedOneTimePasswords ($secret, $timestamp, $digits, $expectedCode, $crypt)
    {
        $code = generateTimebased($secret, $timestamp, $digits, $crypt);
        $this->assertEquals($expectedCode, $code);
    }

    public static function getHTOPProvider ()
    {
        return array(
            array('12345678901234567890', 0, 6, '755224'),
            array('12345678901234567890', 1, 6, '287082'),
            array('12345678901234567890', 2, 6, '359152'),
            array('12345678901234567890', 3, 6, '969429'),
            array('12345678901234567890', 4, 6, '338314'),
            array('12345678901234567890', 5, 6, '254676'),
            array('12345678901234567890', 6, 6, '287922'),
            array('12345678901234567890', 7, 6, '162583'),
            array('12345678901234567890', 8, 6, '399871'),
            array('12345678901234567890', 9, 6, '520489'),
        );
    }

    public static function getTOTPProvider ()
    {
        return array(
            array('12345678901234567890', 59, 8, '94287082', 'sha1'),
            array('12345678901234567890123456789012', 59, 8, '46119246', 'sha256'),
            array('1234567890123456789012345678901234567890123456789012345678901234', 59, 8, '90693936', 'sha512'),

            array('12345678901234567890', 1111111109, 8, '07081804', 'sha1'),
            array('12345678901234567890123456789012', 1111111109, 8, '68084774', 'sha256'),
            array('1234567890123456789012345678901234567890123456789012345678901234', 1111111109, 8, '25091201', 'sha512'),

            array('12345678901234567890', 1111111111, 8, '14050471', 'sha1'),
            array('12345678901234567890123456789012', 1111111111, 8, '67062674', 'sha256'),
            array('1234567890123456789012345678901234567890123456789012345678901234', 1111111111, 8, '99943326', 'sha512'),

            array('12345678901234567890', 1234567890, 8, '89005924', 'sha1'),
            array('12345678901234567890123456789012', 1234567890, 8, '91819424', 'sha256'),
            array('1234567890123456789012345678901234567890123456789012345678901234', 1234567890, 8, '93441116', 'sha512'),

            array('12345678901234567890', 2000000000, 8, '69279037', 'sha1'),
            array('12345678901234567890123456789012', 2000000000, 8, '90698825', 'sha256'),
            array('1234567890123456789012345678901234567890123456789012345678901234', 2000000000, 8, '38618901', 'sha512'),

            array('12345678901234567890', 20000000000, 8, '65353130', 'sha1'),
            array('12345678901234567890123456789012', 20000000000, 8, '77737706', 'sha256'),
            array('1234567890123456789012345678901234567890123456789012345678901234', 20000000000, 8, '47863826', 'sha512'),
        );
    }
}
