<?php
namespace Crunch\Base32;

const RFC3548 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567';

/**
 * Encodes a binary string into Base32
 *
 * RFC3548 http://www.ietf.org/rfc/rfc3548.txt compliant
 *
 * @param string      $data binary data
 * @param string|null $characterSet
 * @return string
 */
function encode ($data, $characterSet = null)
{
    $characterSet = $characterSet ?: RFC3548;
    \assert('\is_string($data) && $data;');
    \assert('\is_null($characterSet) || \is_string($characterSet);');
    \assert('\strlen($characterSet) == 32;');

    $result = '';

    foreach (str_split($data, 5) as $block) {
        $binary = '';

        foreach (str_split($block) as $byte) {
            $binary .= str_pad(decbin(ord($byte)), 8, 0, \STR_PAD_LEFT);
        }

        $blockString = '';
        foreach (str_split($binary, 5) as $code) {
            $char = $characterSet[(int) bindec($code)];
            $blockString .= $char;
        }
        $blockString = str_pad($blockString, 8, '=', \STR_PAD_RIGHT);
        $result .= $blockString;
    }
    return $result;
}

/**
 * Decodes Base32-encoded binary data back into its binary form
 *
 * RFC3548 http://www.ietf.org/rfc/rfc3548.txt compliant
 *
 * @param string      $string
 * @param string|null $characterSet
 * @return string binary data
 */
function decode ($string, $characterSet = null)
{
    $characterSet = $characterSet ? : RFC3548;
    \assert('\is_string($string) && $string;');
    \assert('\is_null($characterSet) || \is_string($characterSet);');
    \assert('\strlen($characterSet) == 32;');

    $result = '';
    foreach (str_split($string, 8) as $block) {
        $binary = '';
        foreach (str_split($block) as $char) {
            if ($char == '=') continue;

            $code = strpos($characterSet, strtoupper($char));
            $binary .= str_pad(decbin($code), 5, 0, \STR_PAD_LEFT);
        }
        foreach (str_split($binary, 8) as $code) {
            $result .= chr(bindec($code));
        }
    }

    return $result;
}
