<?php
namespace Crunch\OneTimePassword;

class TimeBasedGenerator implements Generator
{
    protected $digits;
    protected $crypt;
    public function __construct ($digits, $crypt = null)
    {
        $this->digits = $digits;
        $this->crypt = $crypt;
    }
    public function create ($secret, $timestamp)
    {
        return generateTimebased($secret, $timestamp, $this->digits, $this->crypt);
    }
}
