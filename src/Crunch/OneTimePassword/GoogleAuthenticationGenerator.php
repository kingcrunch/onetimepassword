<?php
namespace Crunch\OneTimePassword;

use Crunch\Base32 as base32;

class GoogleAuthenticationGenerator extends TimeBasedGenerator
{
    public function __construct ()
    {
        parent::__construct(6, CRYPT_SHA1);
    }

    /**
     * @param string $secret base32-encoded
     * @param int $timestamp
     * @return string
     */
    public function create ($secret, $timestamp)
    {
        return parent::create(base32\decode($secret), $timestamp);
    }
}
