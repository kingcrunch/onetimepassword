<?php
namespace Crunch\OneTimePassword;

interface Generator
{
    /**
     * Creates a new OTP based on the state of the class and the given secret and factor
     *
     * Both format and content of the secret and moving factor depends on the
     * implementation. Usually (if not mentioned otherwise) the secret is a binary
     * string and the moving factor a counter.
     *
     * @param string $secret
     * @param int $movingFactor
     * @return string
     */
    public function create ($secret, $movingFactor);
}
