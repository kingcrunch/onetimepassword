<?php
namespace Crunch\OneTimePassword;

class HMACGenerator implements Generator
{
    protected $digits;
    protected $truncationOffset;
    protected $crypt;

    function __construct ($codeDigits, $truncationOffset = null, $crypt = null)
    {
        $this->digits = $codeDigits;
        $this->truncationOffset = $truncationOffset;
        $this->crypt = $crypt;
    }

    public function create ($secret, $movingFactor)
    {
        return generateHMAC($secret, $movingFactor, $this->digits, $this->truncationOffset, $this->crypt);
    }
}
